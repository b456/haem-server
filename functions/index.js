// Firebase
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const serviceAccount = require('./service-account-key.json');
const firebaseConfig = JSON.parse(process.env.FIREBASE_CONFIG);
firebaseConfig.credential = admin.credential.cert(serviceAccount);

// Verify Facebook Account Kit Token
const hmac_sha156 = require('crypto-js/hmac-sha256');
const request = require('request');
